import pyodbc
import uvicorn
from fastapi import FastAPI, HTTPException, Depends
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel
import jwt
from fastapi.encoders import jsonable_encoder
import bcrypt
from datetime import datetime

origins = ["*"]
app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


def connect_245():
    SERVER = '192.168.1.245'
    DATABASE = 'DtradeProduction'
    USERNAME = 'prog3'
    PASSWORD = 'mnb'
    try:
        connString = f'DRIVER={{ODBC Driver 18 for SQL Server}};SERVER={SERVER};DATABASE={DATABASE};UID={USERNAME};PWD={PASSWORD};TrustServerCertificate=yes'
        conn = pyodbc.connect(connString)
        print("Kết nối thành công!")
        return conn  # Trả về đối tượng kết nối
    except Exception as e:
        print("Kết nối thất bại:", e)
        return None

def connect_local():
    SERVER = '192.168.88.140'
    DATABASE = 'DtradeProduction'
    USERNAME = 'sa'
    PASSWORD = 'mnb'
    try:
        connString = f'DRIVER={{ODBC Driver 18 for SQL Server}};SERVER={SERVER};DATABASE={DATABASE};UID={USERNAME};PWD={PASSWORD};TrustServerCertificate=yes'
        conn = pyodbc.connect(connString)
        print("Kết nối thành công!")
        return conn  # Trả về đối tượng kết nối
    except Exception as e:
        print("Kết nối thất bại:", e)
        return None


# Login
# Cấu hình JWT
SECRET_KEY = "phucdethuong"
ALGORITHM = "HS256"
# Mã hóa mật khẩu trước khi lưu vào cơ sở dữ liệu
# Hàm băm mật khẩu và trả về chuỗi băm và muối
def hash_password(password: str) -> tuple:
    salt = bcrypt.gensalt()
    hashed_password = bcrypt.hashpw(password.encode('utf-8'), salt)
    return hashed_password.decode('utf-8'), salt

# Hàm xác minh mật khẩu
def verify_password(plain_password: str, hashed_password: str, salt: bytes) -> bool:
    return bcrypt.checkpw(plain_password.encode('utf-8'), hashed_password.encode('utf-8'))

# Lớp mô hình Pydantic cho thông tin đăng nhập
class Loginclass(BaseModel):
    username: str
    password: str

# Hàm lấy thông tin người dùng từ cơ sở dữ liệu
def get_user(username: str):
    cursor = connect_local().cursor()
    query = f"SELECT username, hashed_password, salt,fac,per FROM Users WHERE username = '{username}'"
    cursor.execute(query)
    row = cursor.fetchone()
    if row:
        return {"username": row[0], "hashed_password": row[1], "salt": row[2], "fac": row[3], "per": row[4]}
    return None

# Endpoint đăng nhập và tạo mã thông báo
@app.post("/login")
async def login_for_access_token(login_item: Loginclass):
    user = get_user(login_item.username)
    if user and verify_password(login_item.password, user['hashed_password'], user['salt']):
        data = {"sub": user['username'] ,"fac": user['fac'], "per": user['per'] }
        encoded_jwt = jwt.encode(data, SECRET_KEY, algorithm=ALGORITHM)
        return { "data" : { 
                    "status_code": 200,
                    "message": "Success",
                    "token": encoded_jwt 
                    }
                }
    else:
        raise HTTPException(status_code=401, detail="Login failed")



# Use question marks as placeholders in the query
@app.get("/")
def getdata():
    return hash_password("123456")


@app.get("/api/v1/dashboardfb/fac={fac}&month={month}&year={year}&dept={dept}")
def getfabricdata(fac:str,month:int,year:int,dept:int):
    try:
        cursor = connect_245().cursor()
        # Execute the stored procedure
        if dept==11 :
            query = f"SET nocount on; EXEC [DtradeProduction].[dbo].InlineFBDashboard 0,{fac},{year},{month}"
            cursor.execute(query)
            rows = cursor.fetchall()  # Lấy tất cả các hàng từ kết quả
            results_list = []
            for row in rows:
                row_dict = {
                    "SupCode": row[0],  # Giả sử cột đầu tiên
                    "RFT": row[1],  # Giả sử cột thứ hai
                    # Thêm các cột khác nếu stored procedure trả về nhiều cột hơn
                }
                results_list.append(row_dict)

            query = f"SET nocount on; EXEC [DtradeProduction].[dbo].InlineFBDashboard 1,{fac},{year},{month}"
            cursor.execute(query)
            rows = cursor.fetchall()  # Lấy tất cả các hàng từ kết quả
            results_list1 = []
            for row in rows:
                row_dict = {
                    "DefectCode": row[0],  # Giả sử cột đầu tiên
                    "DefectName": row[1],  # Giả sử cột thứ hai
                    "Desc1": row[2],  # Giả sử cột thứ 3
                    "TTP": row[3],  # Giả sử cột thứ 4
                    "Per": round(row[4],2),  # Giả sử cột thứ 5
                    # Thêm các cột khác nếu stored procedure trả về nhiều cột hơn
                }
                results_list1.append(row_dict)

            query = f"SET nocount on; EXEC [DtradeProduction].[dbo].InlineFBDashboard 2,{fac},{year},{month}"
            cursor.execute(query)
            rows = cursor.fetchall()  # Lấy tất cả các hàng từ kết quả
            results_list2 = []
            # unique_defect_codes = set()

            for row in rows:
                # defect_code = row[0]
                txt=row[2].split(".")
                img=txt[0]
                # if defect_code not in unique_defect_codes:
                #     unique_defect_codes.add(defect_code)

                row_dict = {
                        "DefectCode": row[0],
                        "DefectName": row[1],
                        "PickLink": 'http://192.168.1.248/FbwarehouseImg/'+img+'.jpg',
                        # Thêm các cột khác nếu stored procedure trả về nhiều cột hơn
                }
                results_list2.append(row_dict)
        else :
            results_list=[]
            results_list1=[]
            results_list2=[]
            cursor = connect_245().cursor()
            # Execute the stored procedure
            query = f"SET nocount on; EXEC [DtradeProduction].[dbo].InlineAccWHDashboard {year},{month},{fac}"
            cursor.execute(query)

            table1=cursor.fetchall()
            table2=[]
            table3=[]
        
            dem = 0
            while (cursor.nextset()):
                if dem == 0 :
                    table2=cursor.fetchall()
                    for row in table2:
                        row_dict = {
                                "DefectName": row[1],  # Giả sử cột đầu tiên
                                "Per": row[3],  # Giả sử cột thứ hai
                            }
                        results_list1.append(row_dict)
                if dem == 1:
                    table3 = cursor.fetchall()
                    for row in table3:
                        row_dict = {
                                "DefectName": row[1],  # Giả sử cột đầu tiên
                                "ImgSrc": 'http://192.168.1.248/AccWHImageDF/'+row[2],  # Giả sử cột thứ hai
                            }
                        results_list2.append(row_dict)
                dem+=1


            for row in table1:
                row_dict = {
                        "TransMonth": row[0],  # Giả sử cột đầu tiên
                        "RFT": row[1],  # Giả sử cột thứ hai
                
                    }
                results_list.append(row_dict)
        # top_5_results = results_list2[:5]
        # Đóng cursor và kết nối
        cursor.close()
        connect_245().close()
        #http://192.168.1.248/FbwarehouseImg/20231102_085329_A1A1108387_F2.jpg
        return {
            "status_code": 200,
            "message": "Success",
            "data": {
                "datalinebar": results_list,
                "datapie": results_list1,
                "datalist": results_list2,
            }
        }
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"Internal Server Error: {str(e)}")
    

# Close the cursor and connection
@app.get("/api/v1/dashboard/fac={fac}&month={month}&year={year}&dept={dept}")
def getfabricdata(fac:str,month:int,year:int,dept:int):
    # 1.Cutting, 2.Heat, 3. Emb, 4. Pad, 5.Bonding, 7.Rega, 8.End, 9.In, 10.Final
    try:

        cursor = connect_245().cursor()
        # Execute the stored procedure
    
        query = f"SET nocount on; EXEC [DtradeProduction].[dbo].[usp_EndlineDecorationReport_GetRFT] {dept},{fac},12,{year},1"
        cursor.execute(query)
        rows = cursor.fetchall()  # Lấy tất cả các hàng từ kết quả
        results_list = []
        for row in rows:
            row_dict = {
                    "Dept": row[0],  # Giả sử cột đầu tiên
                    "FacLine": row[1],  # Giả sử cột thứ hai
                    "TransMonth": row[3],  # Giả sử cột thứ hai
                    "TotalBundleQty": row[4],  # Giả sử cột thứ hai
                    "TotalDefectQty": row[5],  # Giả sử cột thứ hai
                    "RFT": row[6],  # Giả sử cột thứ hai
                    "target": row[7],  # Giả sử cột thứ hai
                    # Thêm các cột khác nếu stored procedure trả về nhiều cột hơn
                }
            results_list.append(row_dict)

        if dept<= 7 :
            query = f"SET nocount on; EXEC [DtradeProduction].[dbo].[usp_EndlineDecorationReport_GetRFT] {dept},{fac},{month},{year},2"
            cursor.execute(query)
            rows = cursor.fetchall()  # Lấy tất cả các hàng từ kết quả
            results_list1 = []
            total_quantity=0
            for row in rows:
                total_quantity += row[3]
                
            for row in rows:
                row_dict = {
                    "TopN": row[0],  # Giả sử cột đầu tiên
                    "DefectName": row[1],  # Giả sử cột thứ hai
                    "DefectQty": row[2],  # Giả sử cột thứ 3
                    "Per": round((row[3]/total_quantity)*100,2),  # Giả sử cột thứ 4
                    "ImgSrc": row[4],  # Giả sử cột thứ 5
                    # Thêm các cột khác nếu stored procedure trả về nhiều cột hơn
                }
                results_list1.append(row_dict)
        else:
            query = f"SET nocount on; EXEC [DtradeProduction].[dbo].[usp_EndlineDecorationReport_GetRFT] {dept},{fac},{month},{year},2"
            cursor.execute(query)
            rows = cursor.fetchall()  # Lấy tất cả các hàng từ kết quả
            results_list1 = []
            total_quantity=0
            for row in rows:
                total_quantity += row[4]
            for row in rows:
                row_dict = {
                    "TopN": row[0],  # Giả sử cột đầu tiên
                    "DefectName": row[1],  # Giả sử cột thứ hai
                    "NameVn": row[2],
                    "DefectQty": row[3],  # Giả sử cột thứ 3
                    "Per": round((row[4]/total_quantity)*100,2),  # Giả sử cột thứ 4
                    "ImgSrc": row[5],  # Giả sử cột thứ 5
                    # Thêm các cột khác nếu stored procedure trả về nhiều cột hơn
                }
                results_list1.append(row_dict)
        # Đóng cursor và kết nối
        cursor.close()
        connect_245().close()
        return {
                    "status_code": 200,
                    "message": "Success",
                    "data": {
                        "datalinebar": results_list,
                        "datapie": results_list1,
                    
                    }
                }
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"Internal Server Error: {str(e)}")

if __name__ == "__main__":
    uvicorn.run("main:app", host="0.0.0.0", port=5000,
                reload=True, reload_dirs='/app')